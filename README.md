
<!-- README.md is generated from README.Rmd. Please edit that file -->

# toolboxfilecharlotte

<!-- badges: start -->
<!-- badges: end -->

The goal of toolboxfilecharlotte is to …

## Installation

You can install the development version of toolboxfilecharlotte like so:

``` r
library(toolboxfilecharlotte)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(toolboxfilecharlotte)
## basic example code

get_one_file_size(file = "analyse_facto.Rmd")
#>                name size extension
#> 1 analyse_facto.Rmd   NA       Rmd

get_all_file_size(path = ".")
#>                            name size extension
#> 1                 ./DESCRIPTION  653          
#> 2                         ./dev 4096          
#> 3                     ./LICENSE   38          
#> 4                  ./LICENSE.md 1067        md
#> 5                         ./man 4096          
#> 6                   ./NAMESPACE  259          
#> 7                           ./R 4096          
#> 8                  ./README.Rmd 1419       Rmd
#> 9                       ./tests 4096          
#> 10 ./toolboxfilecharlotte.Rproj  374     Rproj
#> 11                  ./vignettes 4096

search_folder_size(path = ".", taille =  500)
#>            name size extension
#> 1 ./DESCRIPTION  653          
#> 2         ./dev 4096          
#> 3  ./LICENSE.md 1067        md
#> 4         ./man 4096          
#> 5           ./R 4096          
#> 6  ./README.Rmd 1419       Rmd
#> 7       ./tests 4096          
#> 8   ./vignettes 4096
```

What is special about using `README.Rmd` instead of just `README.md`?
You can include R chunks like so:

``` r
summary(cars)
#>      speed           dist       
#>  Min.   : 4.0   Min.   :  2.00  
#>  1st Qu.:12.0   1st Qu.: 26.00  
#>  Median :15.0   Median : 36.00  
#>  Mean   :15.4   Mean   : 42.98  
#>  3rd Qu.:19.0   3rd Qu.: 56.00  
#>  Max.   :25.0   Max.   :120.00
```

You’ll still need to render `README.Rmd` regularly, to keep `README.md`
up-to-date. `devtools::build_readme()` is handy for this. You could also
use GitHub Actions to re-render `README.Rmd` every time you push. An
example workflow can be found here:
<https://github.com/r-lib/actions/tree/v1/examples>.

You can also embed plots, for example:

<img src="man/figures/README-pressure-1.png" width="100%" />

In that case, don’t forget to commit and push the resulting figure
files, so they display on GitHub and CRAN.

---
title: "flat_first.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(tools)
library(dplyr)
library(purrr)
```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```


# get_one_file_size

Prend un nom de fichier en parametre, et renvoie un data.frame avec son nom et sa taille
    
```{r function-get_one_file_size}

#' Prend un nom de fichier en parametre, et renvoie un data.frame avec son nom et sa taille
#'
#' @param file un nom de fichier
#'
#' @return un data.frame avec nom et taille du fichier
#' @export
#' @importFrom tools file_ext
#'
#' @examples
get_one_file_size <- function(file){
  
  name <- file
  
  size <- file.size(file)
  
  extension <- file_ext(file)
  
  return(data.frame(
    name = name,
    size = size,
    extension = extension))
  
}
```
  
```{r example-get_one_file_size}

get_one_file_size(file = "analyse_facto.Rmd")

```
  
```{r tests-get_one_file_size}
test_that("get_one_file_size works", {
  
  expect_true(inherits(get_one_file_size(file = "analyse_facto.Rmd"), "data.frame")) 
  
})
```
  

# get_all_file_size

Prend un chemin de dossier en parametre et map la fonction precedente pour n obtenir qu un seul tableau

```{r function-get_all_file_size}

#' Prend un chemin de dossier en parametre et map la fonction precedente pour n obtenir qu un seul tableau
#'
#' @param path chemin
#'
#' @return data.frame avec name size extension for each file
#' @export
#' @importFrom purrr map_dfr
#'
#' @examples
get_all_file_size <- function(path){
    
  ma_liste_fichiers <- list.files(path, full.names = TRUE)
  
  return(map_dfr(.x = ma_liste_fichiers, .f = get_one_file_size))
  
}
```
  
```{r example-get_all_file_size}

get_all_file_size(path = ".")

get_all_file_size(path = system.file(package = "toolboxfilecharlotte"))

```
  
```{r tests-get_all_file_size}
test_that("get_all_file_size works", {
  
  expect_true(inherits(get_all_file_size(path = "."), "data.frame")) 
  
})
```
  

# search_folder_size
 
Prend en parametre un nom de dossier et une taille et donne le tableau avec la taille des objets apres avoir filtre les fichiers plus grands que la taille

```{r function-search_folder_size}

#' Prend en parametre un nom de dossier et une taille et donne le tableau avec la taille des objets apres avoir filtre les fichiers plus grands que la taille
#'
#' @param path nom de dossier
#' @param taille taille
#'
#' @return tableau
#' @export
#' @importFrom dplyr filter
#'
#' @examples
search_folder_size <- function(path, taille){
    
  folder <- get_all_file_size(path)
  
  folder_size <- folder |> 
    filter(size > taille)
  
  return(folder_size)
}
```
  
```{r example-search_folder_size}

search_folder_size(path = ".", taille =  500)

```
  
```{r tests-search_folder_size}
test_that("search_folder_size works", {
  
  expect_true(inherits(search_folder_size(path = ".", taille =  500), "data.frame")) 
  
})
```
  




```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_toolboxfilecharlotte.Rmd", vignette_name = "toolboxfilecharlotte")
```
